# Zadanie: Samochodowy licznik prędkości średniej

Twoim zadaniem jest napisanie implementacji klasy **CarSpeedMeter**, która będzie wyliczała średnią prędkość podróży w oparciu o dane pochodzące z czujnika prędkości chwilowej.

Dane z czujnika prędkości możesz pobrać odwołując się do adresu http://danhoss.vdl.pl/get-velocity.php
przekazując GETem zmienną _t_ o wartości odpowiadającej liczbie sekund, które minęły od początku jazdy.
Projektując klasę pamiętaj o tym, że dane z czujnika będą spływały w trakcie całej podróży, więc nie wiadomo początkowo ile ich będzie (długość podróży jest nieznana i może wynieść nawet kilkadziesiąt godzin). Uwzględnij też to, że nie zawsze będzie można odpytać czujnik ze stałym interwałem (np. co 1s), bo np. komputer pokładowy będzie zajęty manewrem zawracania korytarzem życia na autostradzie po wypadku (co ma większy priorytet). Możesz zasymulować to pobierając dane z czujnika co `1 + rand(0,5)` sekund.

Przykład:
> Jeśli zaczynasz podróż z prędkością 0 km/s i przez pierwszą minutę przyspieszasz jednostajnie do 100km/h a następnie jedziesz jedną minutę z prędkością 100km/h to średnia prędkość podróży po 2 minutach powinna wynieść 75km/h.
> Utrzymując dalej prędkość przez kolejne 8 minut na poziomie 100km/h, łącznie po 10 minutach podróży klasa powinna zwrócić wartość 95km/h.

Sugestia:
> Nie chodzi nam o rozwiązanie, które będzie podawało wynik z dokładnością do ósmego miejsca po przecinku,
> ale o rozwiązanie, które nie będzie zwiększało swoich wymagań pamięciowych drastycznie wraz ze wzrostem ilości danych (algorytm in situ).
> Innymi slowy, przechowywanie wszytskich próbek w tablicy i zrobienie na końcu `array_sum($samples)/count($samples)`
> odpada.

Wykorzystując napisaną klasę policz średnią prędkość dla podróży zaczynającej się w czasie _t = 0_ i trwającej 14m 2s z krokiem _ㅿt = 1s_ (czyli 842 próbki).
Następnie poliść średnią prędkość dla podróży zaczynającej sie w czasie _t = 0_ i trwającej 14m 2s z krokiem _ㅿt = 1s_ ale w ostatniej sekundzie wykonaj milion próbek (czyli 1 000 841 próbek, z czego 1M w ostatniej sekundzie).

Klasa **CarSpeedMeter** powinna korzystać z adaptera do pobierania danych (zaimplementuj trzy adaptery: jeden **RestAdapter** - do pobierania danych z serwisu opisanego powyżej, **FileAdapter** do pobierania danych z pliku oraz **DatabaseAdapter** 
do pobierania odczytów z bazy danych (do wyboru wedle uznania Redis, Elastic, MongoDb, MySQL, MsSQL))

Oprócz klasy **CarSpeedMeter** stwórz klasę **CarSpeedGaugeAbstract** w dwóch implementacjach: **DigitalGauge** i **AnalogGauge** będą to wskaźniki prezentujące chwilową prędkość - cyfrowy niech wskazuje prędkość jako liczbę w mph a analogowy
niech wskazuje kąt na jakim ma się ustawić wskazówka wskaźnika prędkości (przyjmując, że 0km/h jest na godzinie 8:00, a 220km/h jest na godzinie 05:00, a skala jest podzielona równomiernie).

Stwórz też klasę **CarAbstract** która będzie wykorzystywała komponenty **CarSpeedMeter** oraz **CarSpeedGauge** i dwie implementacje **OpelVectraLpg** i **AudiB4A7**. Niech Opel korzysta z wskaźnika analogowego a Audi - cyfrowego.
Do tworzenia instancji konkretnych klas stwórz i użyj klasy buildera **CarBuilder** który stworzy obiekty samochodów w zależności od podanej nazwy.

Niech miernik i wskaźnik samochodu komunikują się za pomocą eventów (po dokonaniu pomiaru miernik rzuca event, do którego jako listener podpięty jest wskaźnik).

Do wszystkich metod które uznasz za istotne napisz testy jednostkowe.

Na koniec napisz skrypt, który wykorzysta stworzone klasy i zasymuluje przebieg trasy dla opla w gazie i audi.